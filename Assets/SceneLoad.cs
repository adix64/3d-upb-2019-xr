﻿//////////////////////////////////////////////////////////////
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoad : MonoBehaviour
{
	public void Open1stPersonVR()
	{
		SceneManager.LoadScene("1stPersVR", LoadSceneMode.Single);
	}
	public void Open3rdPersonVR()
	{
		SceneManager.LoadScene("3rdPersVR", LoadSceneMode.Single);
	}
	public void OpenMecanimIntro()
	{
		SceneManager.LoadScene("mecanimIntro", LoadSceneMode.Single);
	}
	public void OpenARimageTarget()
	{
		SceneManager.LoadScene("ARimageTarget", LoadSceneMode.Single);
	}
}
