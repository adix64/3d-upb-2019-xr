﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdPersonCamera : MonoBehaviour
{
	public Transform target;
	public float radius, mouseSensitivityX, mouseSensitivityY;

	private float phi = 0f, theta = 0f; //coordontele sferice
	private float x, y, z; //coordonatele carteziene locale ale sferei pe care orbiteaza camera

    // Update is called once per frame
    void LateUpdate()
    {
		theta += Input.GetAxis("Mouse Y") * mouseSensitivityX;
		phi -= Input.GetAxis("Mouse X") * mouseSensitivityY;
		//limiteaza theta a.i. camera sa orbiteze pe suprafata unei sfere cu polii taiati (sa nu faca tumbe)
		theta = Mathf.Clamp(theta, -Mathf.PI * 0.25f, Mathf.PI * 0.06f);

		float positiveTheta = theta + Mathf.PI * 0.5f; //theta in [0...pi]
		//coordonate sferice -> coordonate carteziene
		x = radius * Mathf.Sin(positiveTheta) * Mathf.Cos(phi);
		y = radius * Mathf.Cos(positiveTheta);
		z = radius * Mathf.Sin(positiveTheta) * Mathf.Sin(phi);

		transform.position = target.position + new Vector3(x, y, z);

		transform.LookAt(target, Vector3.up); //orienteaza camera cu forward catre target si up = world Y
	}
}
