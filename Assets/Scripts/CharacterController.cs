﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Scriptul care controleaza un personaj cu FSMAC(Animator)
public class CharacterController : MonoBehaviour
{
	public float extraTurnSpeed = 10f;
	public float YjumpPower = 5f, XZjumpPower = 2f;

	bool grounded = false;
	private Animator animator;
	private Rigidbody rigidBody;
	private Vector3 targetDir;
	private float animatorVelX, animatorVelZ;

	//////////////////////////////////////////////////////////////////////////////////////////

	void Awake()
    {
		animator = GetComponent<Animator>();
		rigidBody = GetComponent<Rigidbody>();
	}

	//////////////////////////////////////////////////////////////////////////////////////////

	void Update()
    {
		//control utilizator cu WASD sau joystick
		float h = Input.GetAxis("Horizontal");
		float v = Input.GetAxis("Vertical");

		//directia in care ne deplasam, in wolrd space
		targetDir = Camera.main.transform.TransformDirection(new Vector3(h, 0, v)).normalized;
		targetDir = (new Vector3(targetDir.x, 0, targetDir.z)).normalized; // deplasare doar in plan orizontal
		//directia in care ne deplasam, in sistemul de coordonate al personajului
		Vector3 characterSpaceDir = transform.InverseTransformDirection(targetDir).normalized;

// FSMAC parameters for character movement on XZ ground plane
	//(params for 2D freeform directional Grounded blendtree)
		float run = 1f;
		if (Input.GetKey(KeyCode.LeftShift))
			run = 0.5f; // walk
		animatorVelX = characterSpaceDir.x * run;
		animatorVelZ = characterSpaceDir.z * run;
//
		//Root Translation
		if (grounded)
		{
			Vector3 velocity = animator.deltaPosition / Time.deltaTime;
			rigidBody.velocity = new Vector3(velocity.x, rigidBody.velocity.y, velocity.z);
														//pentru cadere/jump^
		}
		
		ExtraRootRotation();
		HandleJumpAndAirborne();
		
	}

	//////////////////////////////////////////////////////////////////////////////////////////

	private void HandleJumpAndAirborne()
	{
		if (!grounded)
		{//daca nu e pe pamant, raycast in jos sa vedem la ce distanta e ceva podea de personaj
			Ray ray = new Ray();
			ray.origin = transform.position + Vector3.up * 0.1f;
			ray.direction = Vector3.down;
			if (Physics.Raycast(ray, out RaycastHit hitInfo))
				if (hitInfo.distance < 0.2f)
					grounded = true;
				else
					grounded = false;
		}

		if (grounded && Input.GetKeyDown(KeyCode.Space))
		{//sare, daca e pe pamant
			animator.SetTrigger("Jump");
			grounded = false;
			rigidBody.AddForce(new Vector3(rigidBody.velocity.x * XZjumpPower,
													YjumpPower,
											rigidBody.velocity.z * XZjumpPower),
											ForceMode.VelocityChange);
		}
	}

	//////////////////////////////////////////////////////////////////////////////////////////

	private void ExtraRootRotation()
	{
		if (targetDir.magnitude == 0f)
			return;

		{
			Vector3 axis = Vector3.Cross(transform.forward, targetDir).normalized;
			float angle = Mathf.Acos(Vector3.Dot(targetDir, transform.forward)) * Mathf.Rad2Deg
						 * Time.deltaTime * extraTurnSpeed;
			transform.rotation = Quaternion.AngleAxis(angle, axis) * transform.rotation;
		}
		//SAU alternativa cu FromToRotation
		{
			//Quaternion desiredRotation = Quaternion.FromToRotation(forwardXZplane, targetDirXZplane) * transform.rotation;
			//transform.rotation = Quaternion.Slerp(transform.rotation, desiredRotation, 0.25f);
			//transform.rotation = Quaternion.Euler(0, transform.rotation.eulerAngles.y, 0);
		}

#if DEBUG
		Debug.DrawLine(transform.position, transform.position + transform.forward, Color.red, 0.05f);
		Debug.DrawLine(transform.position, transform.position + targetDir, Color.white, 0.05f);
#endif
	}

	//////////////////////////////////////////////////////////////////////////////////////////

	private void OnAnimatorMove()
	{
		//update FSMAC parameters
		animator.SetFloat("velX", animatorVelX, 0.2f, Time.deltaTime);
		animator.SetFloat("velZ", animatorVelZ, 0.2f, Time.deltaTime);
		animator.SetBool("Grounded", grounded);
	}
}
