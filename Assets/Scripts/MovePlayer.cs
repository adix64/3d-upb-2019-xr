﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePlayer : MonoBehaviour
{

	Transform camTransform;

	// Use this for initialization
	void Start()
	{
		camTransform = Camera.main.transform;
	}

	// Update is called once per frame
	void Update()
	{
		float x = Input.GetAxis("Vertical");
		float z = -Input.GetAxis("Horizontal");

		transform.position += Time.deltaTime * (x * camTransform.right + z * camTransform.forward) * 30f;
	}
}