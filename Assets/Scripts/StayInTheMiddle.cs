﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Obiect ce reprezinta centroidul unui 'cluster de obiecte',
//folosit pe post de 3rd person camera target ca aceasta sa cuprinda 'toate personajele'
public class StayInTheMiddle : MonoBehaviour
{
	public Transform[] targets;
	public Vector3 offset;

    // Update is called once per frame
    void Update()
    {
		Vector3 sum = Vector3.zero;
		for (int i = 0; i < targets.Length; i++)
			sum += targets[i].position;

		transform.position = sum / (float)targets.Length + offset;
    }
}
