﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VR3rdPersonCam : MonoBehaviour
{
	public Transform camTarget;
	Transform camTransform;
    // Start is called before the first frame update
    void Start()
    {
		camTransform = Camera.main.transform;
    }

    // Update is called once per frame
    void LateUpdate()
    {   //ca sa se uite catre target...
		//pozitia camerei = pozitia targetului  - directia inainte a camerei
		transform.position = camTarget.position - camTransform.forward * 3f;
    }
}
