﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeColorByDot : MonoBehaviour
{
    Transform cameraTransform;
    MeshRenderer meshRenderer;
    Color initialColor;
    // Start is called before the first frame update
    void Start()
    {
        meshRenderer = GetComponent<MeshRenderer>();
        initialColor = meshRenderer.material.color;
        cameraTransform = Camera.main.transform;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 D = (transform.position - cameraTransform.position).normalized;
        float dotProd = Vector3.Dot(cameraTransform.forward, D);
        meshRenderer.material.color = Color.Lerp(initialColor, Color.red, Mathf.Pow(Mathf.Abs(dotProd), 20f));
    }
}
