﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
public class arModeSwitch : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        SwitchToVuforia();
    }
    void Awake()
    {
        //get your video material component
         //VideoMaterial myVideoMaterial = GetComponent<VideoMaterial>();

        //Look for a shader called "VideoBackground" and apply it to the shader material of the component
        //myVideoMaterial.material.shader = Shader.Find("Custom/VideoBackground");

        Destroy(this);//this will remove this script after executing it, just looks a bit cleaner in my opinion but no necessary
    }
    public void SwitchToVuforia()
    {
        StartCoroutine(LoadDevice("Vuforia"));
    }
    // Update is called once per frame
    IEnumerator LoadDevice(string newDevice)
    {
        UnityEngine.XR.XRSettings.LoadDeviceByName(newDevice);
        yield return null;
        UnityEngine.XR.XRSettings.enabled = true;
    }
}
