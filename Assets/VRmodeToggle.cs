﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VRmodeToggle : MonoBehaviour
{
	private void Start()
	{
		ToggleVR();		
	}
	void ToggleVR()
	{
		if (UnityEngine.XR.XRSettings.loadedDeviceName == "Cardboard")
			StartCoroutine(LoadDevice("None"));
		else
			StartCoroutine(LoadDevice("Cardboard"));
	}


	IEnumerator LoadDevice(string newDevice)
	{
		UnityEngine.XR.XRSettings.LoadDeviceByName(newDevice);
		yield return null;
		UnityEngine.XR.XRSettings.enabled = true;
	}
}
