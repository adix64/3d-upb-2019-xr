# *3D UPB XR Workshop*

Salut,
Va invitam sa clonati acest repository. Forks, Issues, contributions, whatever you like.

   • Google Cardboard Tutorial for Unity   https://docs.google.com/document/d/19dyBfcO64-YUR99_3wpms9gCSaIzJei5WxPVkEhdOZ0/edit

   • Get Started with AR in Unity: Vuforia   https://library.vuforia.com/articles/Training/getting-started-with-vuforia-in-unity.html
       - Vuforia License Key Manager   https://library.vuforia.com/articles/Solution/How-To-add-a-License-Key-to-your-Vuforia-App

   • Android APK build example   https://drive.google.com/open?id=1SMlXAqKZj4bcDuXRnQnQ1evHqc6ypNHI

#### *Mecanim Animator Controller utilizat in proiect, explicatie vizuala cu noduri expandate:* 
<img src="https://gitlab.com/adix64/3d-upb-2019-xr/raw/master/Assets/MecanimExplaination.png" alt="Mecanim Diagram" width="800" class="center"/>

#### *AR marker pentru image target din proiect* 
<img src="https://gitlab.com/adix64/3d-upb-2019-xr/raw/master/Assets/AR%20marker.jpg" alt="ARmarker" width="150"/>


### *Resurse utile:*

   • TurboSquid   https://www.turbosquid.com/

   • Mixamo   https://www.mixamo.com

   • Toggle Unity VR mode on/off   https://answers.unity.com/questions/1355443/how-to-toggle-vr-cardboard-on-and-off-in-56.html

   • OpenGameArt.org   https://opengameart.org
   
   • Game Models Resource   https://www.models-resource.com

   • *cgpeers* :wink:
  